jQuery.noConflict(true);
if (typeof jQuery == 'undefined') { 
    if (typeof $ == 'function') {
        var thisPageUsingOtherJSLibrary = true;
    }
    function getScript(url, success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
            done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            };
        };
        head.appendChild(script);
    };
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', function() {
        if (typeof jQuery=='undefined') {
        } else {
            if (thisPageUsingOtherJSLibrary) {
                runjQueryScript();
            } else {
                runjQueryScript();
            }
        }
    });
} else { 
    console.log('jQuery library found.');
    runjQueryScript();
};

function runjQueryScript(){

jQuery.fn.select2List = function(options) {
  return this.each(function() {
    var $ = jQuery;
    var select = $(this);
    var multiselect = select.attr('multiple');
    select.hide();
    var buttonsHtml = $('<div class="dropdown"><button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true"><span class="caret"></span></button></div>');
    var selectIndex = 0;
    var addOptGroup = function(optGroup) {
      if (optGroup.attr('label')) {
        buttonsHtml.append('<strong>' + optGroup.attr('label') + '</strong>');
      }
      var ulHtml = $('<ul class="dropdown-menu dropdown-menu-right" role="menu">');
      optGroup.children('option').each(function() {
        var liHtml = $('<li role="presentation"></li>');
        if ($(this).attr('disabled') || select.attr('disabled')) {
          liHtml.addClass('disabled');
          liHtml.append('<span>' + $(this).html() + '</span>');
        } else {
            liHtml.append('<a href="#" data-select-index="' + selectIndex + '">' + $(this).html() + '</a>');
          }
        // Mark current selection as "picked"
        if ((!options || !options.noDefault) && $(this).attr('selected')) {
          liHtml.children('a, span').addClass('picked');
        }
        ulHtml.append(liHtml);
        selectIndex++;
      });
      buttonsHtml.append(ulHtml);
    }
    var optGroups = select.children('optgroup');
    if (optGroups.length == 0) {
      addOptGroup(select);
    } else {
        optGroups.each(function() {
          addOptGroup($(this));
        });
      }
    select.after(buttonsHtml);
    buttonsHtml.find('a').click(function(e) {
      e.preventDefault();
      var clickedOption = $(select.find('option')[$(this).attr('data-select-index')]);
      if (multiselect) {
        if (clickedOption.attr('selected')) {
          $(this).removeClass('picked');
          clickedOption.removeAttr('selected');
        } else {
            $(this).addClass('picked');
              clickedOption.attr('selected', 'selected');
          }
      } else {
          if ($(this).hasClass('picked')) {
            $(this).removeClass('picked');
            clickedOption.removeAttr('selected');
          } else {
              buttonsHtml.find('a, span').removeClass('picked');
              $(this).addClass('picked');
              clickedOption.attr('selected', 'selected');
            }
      }
      select.trigger('change');
    });
  });
};

// Adding selectList class to function dropdown selections.
var functionBox = $('.functionbox span select').addClass('selectList');
var docList = $('table.treedisplay tbody tr').children().eq(1).addClass('docList');

// using plug-in to build un-ordered list and creating bootstrap dropdown menu.
$('.selectList').select2List();

//Showing function box when row is hovered over.
$('table.list tr')
  .on('mouseover', function () {
    $(this).find('.dropdown').css('visibility', 'visible');})
  .on('mouseout', function () {
    $(this).find('.dropdown').css('visibility', 'hidden');})
  .on('contextmenu', function(e) {
    /*e.preventDefault();*/
  });


//Documents Feature
$('.docList').children().eq(0).addClass('action-links');

// manage folders
$('.doc td.treedisplay table.action td').addClass('mg-folders');
$('.mg-folders a').addClass('btn btn-xs btn-success text-white');

var ul = $("<div class='dropdown btn-group docList-menu'><a id='quickLink' href='javascript:viewIndex();' class='btn btn-default btn-sm'><i id='quick-btn' class='fa fa-list-alt'></i></a><button class='btn btn-default btn-sm dropdown-toggle' type='button' id='dropdownDocAction' data-toggle='dropdown' aria-expanded='true'><span class='caret'></span></button>");

$(".action-links tr").each(function(){
  var li = $("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownDocAction'>")
  $("th, td", this).each(function(){
    var span = $("<li role='presentation'>").html(this.innerHTML);
    li.append(span);
  });
  ul.append(li);
})    
// replacing table with list.
$(".action-links").replaceWith(ul); 

$('.required').addClass('text-danger strong').removeClass('required');
$('form[name="DocDisplay"] table.action td[colspan="2"], form[name="DocEdit"] table.action td').addClass('actionList');
$("form[name='DocEdit']").addClass('addDocument');

$(".actionList").each(function(){
  var li = $("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownDocAction'>")
  $(this).each(function(){
    var span = $("<li role='presentation'>").html(this.innerHTML);
    li.append(span);
  });
  ul.append(li).append;
});
// replacing table with list.
$(".actionList").replaceWith(ul); 

$('.dropdown ul li').contents().filter( function () {
  return this.nodeType == 3 
}).remove();

// dropdown menu for table actions in documents feature.
$(".dropdown-menu li a[href='javascript:addLink();']").on('click', function() {
  $('#quick-btn').attr('class', 'fa fa-link');
  $('#quickLink').attr('href', 'javascript:addLink();');
});
$(".dropdown-menu li a[href='javascript:viewIndex();']").on('click', function() {
  $('#quick-btn').attr('class', 'fa fa-list-alt');
  $('#quickLink').attr('href', 'javascript:viewIndex();');
});
$(".dropdown-menu li a[href='javascript:selectColumns();']").on('click', function() {
  $('#quick-btn').attr('class', 'fa fa-exchange');
  $('#quickLink').attr('href', 'javascript:selectColumns();');
});
$(".dropdown-menu li a[href='javascript:getSort();']").on('click', function() {
  $('#quick-btn').attr('class', 'fa fa-filter');
  $('#quickLink').attr('href', 'javascript:getSort();');
});
$(".dropdown-menu li a[href='javascript:selectFolder();']").on('click', function() {
 $('#quick-btn').attr('class', 'fa fa-folder-o');
 $('#quickLink').attr('href', 'javascript:selectFolder();');
});

//Area Edit Links.
$('.action a[href*="javascript:areaList"]').before('<i class="fa fa-arrow-circle-left pr5"></i>');
$('.action a[href*="javascript:areaOperation"]').before('<i class="fa fa-list pr5"></i>');
$('.action a[href*="javascript:addMembers"]').before('<i class="fa fa-user-plus pr5"></i>');
$('.action a[href*="javascript:memberList"]').before('<i class="fa fa-file-text-o pr5"></i>');

//Matter Profile
$('form[name="AreaEdit"] table.head').after('<div id="AreaProfileTabs" role="tabpanel" class="pt25"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#profile" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li><li role="presentation"><a href="#preferences" aria-controls="profile" role="tab" data-toggle="tab">Preferences</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="profile"></div><div role="tabpanel" class="tab-pane" id="preferences"></div></div></div>');

$('form[name="UserProfile"] .head').before('<div id="UserProfileTabs" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#myprofile" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li><li role="presentation"><a href="#preferences" aria-controls="profile" role="tab" data-toggle="tab">Preferences</a></li><li role="presentation"><a href="#email" aria-controls="email" role="tab" data-toggle="tab">Notification Settings</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="myprofile"><div  id="profile"></div></div><!-- End of Profile pane --><div role="tabpanel" class="tab-pane" id="preferences"></div><div role="tabpanel" class="tab-pane" id="email"></div></div></div>');

//Member Edit
$('.action a[href*="Import"]').before('<i class="fa fa-retweet pr5"></i>');
$('.action a[href*="Email"]').before('<i class="fa fa-envelope-o pr5"></i>');
$('.action a[href*="javascript:memberOperation();"]').before('<i class="fa fa-list pr5"></i>');
$('.action a[href*="javascript:selectColumns"]').before('<i class="fa fa-columns pr5"></i>');
$('.action a[href*="javascript:openSort();"]').before('<i class="fa fa-filter pr5"></i>');
$('.action a[href*="javascript:addArea();"]').before('<i class="fa fa-sitemap pr5"></i>');
$('.action a[href*="javascript:addAreaGroup();"]').before('<i class="fa fa-bolt pr5"></i>');
$('#divAdmin').addClass('pb10');

$('form[name="MemberEdit"] table.action').after('<div id="MemberEditTabs" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#profile" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li><li role="presentation"><a href="#preferences" aria-controls="profile" role="tab" data-toggle="tab">Preferences</a></li><li role="presentation"><a href="#email" aria-controls="email" role="tab" data-toggle="tab">Notification Settings</a></li><li role="presentation"><a href="#adminRights" aria-controls="adminRights" role="tab" data-toggle="tab">Admin Privileges</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="profile"></div><!-- End of Profile pane --><div role="tabpanel" class="tab-pane" id="preferences"></div><div role="tabpanel" class="tab-pane" id="email"></div><div role="tabpanel" class="tab-pane" id="adminRights"></div></div></div>');

$('#divPref, #divEmail, #divRights, #divAdmin').removeAttr('style');
// Member Profile, Member Edit
$('#divAdmin').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#profile').append($(this));
});
// Member Profile, Member Edit Tab
$('#divData').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#profile').append($(this));
});
// Member Preferences Tab
$('#divPref').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#preferences').append($(this));
});
//  Email Preferences Tab
$('#divEmail').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#email').append($(this));
});

// Admin Rights
$('#divRights').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#adminRights').append($(this));
});

//Functional Names Feature
$('form[name="SiteNames"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#functional" aria-controls="home" role="tab" data-toggle="tab">Functional Names</a></li><li role="presentation"><a href="#defaultmenus" aria-controls="profile" role="tab" data-toggle="tab">Default Menu Names</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="functional"></div><div role="tabpanel" class="tab-pane" id="defaultmenus"></div></div></div>');

$('#divMenu').removeAttr('style');
$('#divFunction').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#functional').append($(this));
});
//Default Menu Names
$('#divMenu').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
          return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#defaultmenus').append($(this));
});

  //Site Rules
$('form[name="SiteRules"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#SelfRegRules" aria-controls="home" role="tab" data-toggle="tab">Self Registration Rules</a></li><li role="presentation"><a href="#NotifyRules" aria-controls="profile" role="tab" data-toggle="tab">Notification Rules</a></li><li role="presentation"><a href="#ItemRules" aria-controls="email" role="tab" data-toggle="tab">Item Rules</a></li><li role="presentation"><a href="#SecurityRules" aria-controls="email" role="tab" data-toggle="tab">Security Rules</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="SelfRegRules"></div><div role="tabpanel" class="tab-pane" id="NotifyRules"></div><div role="tabpanel" class="tab-pane" id="ItemRules"></div><div role="tabpanel" class="tab-pane" id="SecurityRules"></div></div></div>');

$('#divNotify, #divItem, #divSecurity').removeAttr('style');
// Self Reg Rules
$('#divReg').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#SelfRegRules').append($(this));
      });
// Notify Rules
  $('#divNotify').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#NotifyRules').append($(this));
      });
// Item Rules
  $('#divItem').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#ItemRules').append($(this));
      });
// Item Rules
  $('#divSecurity').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#SecurityRules').append($(this));
      });

  //Site Settings 

   $('form[name="SiteSettings"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#Configuration" aria-controls="home" role="tab" data-toggle="tab">Configuration</a></li><li role="presentation"><a href="#EULA" aria-controls="profile" role="tab" data-toggle="tab">End User License Agreement (EULA)</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="Configuration"></div><div role="tabpanel" class="tab-pane" id="EULA"></div></div></div>');

$('#divEULA').removeAttr('style');
// Site Configuration
  $('#divSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#Configuration').append($(this));
      });
// EULA
  $('#divEULA').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#EULA').append($(this));
      });

//Login Page Settings, CSS Data, Layout Data
$('form[name="SiteLoginPage"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#Main" aria-controls="home" role="tab" data-toggle="tab">Login Page Settings</a></li><li role="presentation"><a href="#SiteImages" aria-controls="profile" role="tab" data-toggle="tab">Style Sheet Images</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="Main"></div><div role="tabpanel" class="tab-pane" id="SiteImages"></div></div></div>');

$('form[name="SiteLayout"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#Main" aria-controls="home" role="tab" data-toggle="tab">Site Layouts</a></li><li role="presentation"><a href="#SiteImages" aria-controls="profile" role="tab" data-toggle="tab">Style Sheet Images</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="Main"></div><div role="tabpanel" class="tab-pane" id="SiteImages"></div></div></div>');

$('form[name="SiteCSS"] table.head').after('<div role="tabpanel"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#Main" aria-controls="home" role="tab" data-toggle="tab">Style Sheets</a></li><li role="presentation"><a href="#SiteImages" aria-controls="profile" role="tab" data-toggle="tab">Style Sheet Images</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="Main"></div><div role="tabpanel" class="tab-pane" id="SiteImages"></div></div></div>');


$('#divImages').removeAttr('style');
$('#divSettings').each(function () {
  $(this).find().each(function () {
    var p = $(this).children().map(function () {
      if ($(this).find('table').length > 0) {
        var subList = $("<div class='two'/>");
        var sP = $(this).find('table').children().map(function () {
        return "<span>" + $(this).html() + "</span>";
        });
        subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
        return $('<div />').append($('<p />').append(subList)).html();
      }
      else {
        return "<div>" + $(this).html() + "</div>";
      }
    });
    list.append("<div>" + $.makeArray(p).join("") + "</div>");
  });
  $('#Main').append($(this));
});
$('#divLayout').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#Main').append($(this));
      });
$('#divCSS').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#Main').append($(this));
      });
$('#divImages').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#SiteImages').append($(this));
      });

//SitePages
$('form[name="SitePages"] table.head').after('<div id="SitePagesTab" role="tabpanel" class="pt10"><!-- Panels --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="SiteHomePages"></div></div>');

  $('#divPage').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#SiteHomePages').append($(this));
      });

//Menu Data
$('form[name="SiteMenu"] table.head').after('<div role="tabpanel"><!-- Panels --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="Main"></div></div></div>');

$('form[name="SiteMenu"] #divSettings').removeAttr('style');
  $('#divMenu').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#Main').append($(this));
      });
    $('form[name="SiteMenu"] #divSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#Main').append($(this));
      });

//Member Preferences
$('form[name="MemberPreferences"] table.head').after('<div id="MemberPreferencesTab" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General Settings</a></li><li role="presentation"><a href="#assignments" aria-controls="assignments" role="tab" data-toggle="tab">Assignment Defaults</a></li><li role="presentation"><a href="#layouts" aria-controls="layouts" role="tab" data-toggle="tab">Layout Settings</a></li><li role="presentation"><a href="#documentsettings" aria-controls="documentsettings" role="tab" data-toggle="tab">Document Settings</a></li><li role="presentation"><a href="#datetime" aria-controls="datetime" role="tab" data-toggle="tab">Date Time Settings</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="general"></div><div role="tabpanel" class="tab-pane" id="assignments"></div><div role="tabpanel" class="tab-pane" id="layouts"></div><div role="tabpanel" class="tab-pane" id="documentsettings"></div><div role="tabpanel" class="tab-pane" id="datetime"></div></div></div>');


$('#divGeneralSettings, #divAssignmentDefaults, #divLayoutSettings, #divDocumentSettings, #divDateTimeSettings').removeAttr('style');
// Member Preferences tab #divGeneralSettings
  $('#divGeneralSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#general').append($(this));
      });
// Member Preferences tab #divAssignmentDefaults
  $('#divAssignmentDefaults').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#assignments').append($(this));
      });
// Member Preference tab #divLayoutSettings
  $('#divLayoutSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#layouts').append($(this));
      });
// Member Preferences tab #divDocumentSettings
  $('#divDocumentSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#documentsettings').append($(this));
      });
// Member Preferences tab #divDateTimeSettings
  $('#divDateTimeSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#datetime').append($(this));
      });

// Matter Preferences
$('form[name="AreaPreferences"] table.head').after('<div id="AreaPreferencesTab" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General Settings</a></li><li role="presentation"><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications Settings</a></li><li role="presentation"><a href="#displaysettings" aria-controls="displaysettings" role="tab" data-toggle="tab">Display Settings</a></li><li role="presentation"><a href="#layouts" aria-controls="layouts" role="tab" data-toggle="tab">Layout Settings</a></li><li role="presentation"><a href="#documentsettings" aria-controls="documentsettings" role="tab" data-toggle="tab">Document Settings</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="general"></div><div role="tabpanel" class="tab-pane" id="notifications"></div><div role="tabpanel" class="tab-pane" id="displaysettings"></div><div role="tabpanel" class="tab-pane" id="layouts"></div><div role="tabpanel" class="tab-pane" id="documentsettings"></div></div></div>');


$('#divGeneralSettings, #divNotificationSettings, #divDisplaySettings, #divLayoutSettings, #divDocumentSettings').removeAttr('style');
// Area Preferences tab #divGeneralSettings
  $('#divGeneralSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#general').append($(this));
      });
// Member Preferences tab #divNotificationSettings
  $('#divNotificationSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#notifications').append($(this));
      });
// Member Preference tab #divDisplaySettings
  $('#divDisplaySettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#displaysettings').append($(this));
      });
// Member Preferences tab #divLayoutSettings
  $('#divLayoutSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#layouts').append($(this));
      });
// Member Preferences tab #divDocumentSettings
  $('#divDocumentSettings').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#documentsettings').append($(this));
      });

// Matter Administration Event Data
$('form[name="AreaEventData"] table.head').after('<div id="AreaAdminEventData" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab">Resources</a></li><li role="presentation"><a href="#colors" aria-controls="colors" role="tab" data-toggle="tab">Colors</a></li><li role="presentation"><a href="#displayfields" aria-controls="displayfields" role="tab" data-toggle="tab">Display Fields</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="resources"></div><div role="tabpanel" class="tab-pane" id="colors"></div><div role="tabpanel" class="tab-pane" id="displayfields"></div></div></div>');

$('#divResources, #divColors, #divDisplay').removeAttr('style');

// Area Admin #divResources
  $('#divResources').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#resources').append($(this));
      });
// Area Admin #divColors
  $('#divColors').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#colors').append($(this));
      });
// Area Admin #divDisplay
  $('#divDisplay').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#displayfields').append($(this));
      });


// Matter Administration Functional Names
$('form[name="AreaNames"] table.head').after('<div id="AreaNamesTabs" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#names" aria-controls="names" role="tab" data-toggle="tab">Functional Names</a></li><li role="presentation"><a href="#features" aria-controls="colors" role="tab" data-toggle="tab">Default Feature Names</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="names"></div><div role="tabpanel" class="tab-pane" id="features"></div></div></div>');

$('#divNames, #divFeature').removeAttr('style');


// Area Functional Names #divNames
  $('#divNames').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#names').append($(this));
      });
// Area Functional Names #divFeature
  $('#divFeature').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#features').append($(this));
      });

// Matter Admin - Matter Data
$('form[name="AreaData"] table.head').after('<div id="AreaNamesTabs" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab">Matter Fields</a></li><li role="presentation"><a href="#defaults" aria-controls="defaults" role="tab" data-toggle="tab">Matter Defaults</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="fields"></div><div role="tabpanel" class="tab-pane" id="defaults"></div></div></div>');


$('#divFields, #divDefaults').removeAttr('style');

// Area Matter Data #divFields
  $('#divFields').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#fields').append($(this));
      });
// Area Matter Data #divDefaults
  $('#divDefaults').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#defaults').append($(this));
      });


// Matter Admin - Custom Fields
$('form[name="AreaCustomFields"] table.head').after('<div id="AreaNamesTabs" role="tabpanel" class="pt10"><!-- Nav tabs --><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#docfields" aria-controls="docfields" role="tab" data-toggle="tab">Document Fields</a></li><li role="presentation"><a href="#eventfields" aria-controls="eventfields" role="tab" data-toggle="tab">Event Fields</a></li></ul><!-- Tab panes --><div class="tab-content"><div role="tabpanel" class="tab-pane active" id="docfields"></div><div role="tabpanel" class="tab-pane" id="eventfields"></div></div></div>');

$('#divDoc, #divEvent').removeAttr('style');

// Area Matter Data #divFields
  $('#divDoc').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#docfields').append($(this));
      });
// Area Matter Data #divDefaults
  $('#divEvent').each(function () {
               $(this).find().each(function () {
                    var p = $(this).children().map(function () {
                        if ($(this).find('table').length > 0) {
                            var subList = $("<div class='two'/>");
                            var sP = $(this).find('table').children().map(function () {
                                return "<span>" + $(this).html() + "</span>";
                            });
                            subList.append("<div>" + $.makeArray(sP).join("") + "</div>");
                            return $('<div />').append($('<p />').append(subList)).html();
                        }
                        else {
                            return "<div>" + $(this).html() + "</div>";
                       }
                    });
                    list.append("<div>" + $.makeArray(p).join("") + "</div>");
                });
               $('#eventfields').append($(this));
      });

}